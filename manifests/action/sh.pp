# Run a shell script as part of a backupninja run
#
define backupninja::action::sh (
  String $ensure              = 'present',
  Integer $order              = 50,
  Optional[Boolean] $validate = undef,
  Optional[String] $content   = undef,
  Optional[String] $source    = undef,
) {

  backupninja::action { "${name}.sh":
    ensure   => $ensure,
    filename => $name,
    order    => $order,
    type     => 'sh',
    content  => $content,
    source   => $source,
    validate => $validate,
  }

}
