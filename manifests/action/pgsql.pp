# Safe PostgreSQL dumps, as part of a backupninja run.
#
define backupninja::action::pgsql (
  String $ensure                         = 'present',
  Integer $order                         = 10,
  Optional[Array] $when                  = undef,
  Optional[Boolean] $validate            = undef,
  # backup config
  String $backupdir                      = '/var/backups/postgres',
  String $databases                      = 'all',
  Boolean $compress                      = true,
  Enum['plain', 'tar', 'custom'] $format = 'plain',
) {

  if empty($databases) {
    fail('Must define one or more databases to backup!')
  }

  $config = {
    'when'      => $when,
    'backupdir' => $backupdir,
    'databases' => $databases,
    'compress'  => $compress,
    'format'    => $format,
  }

  backupninja::action { "${name}.pgsql":
    ensure   => $ensure,
    filename => $name,
    order    => $order,
    type     => 'pgsql',
    content  => epp('backupninja/action.epp', { 'config' => $config } ),
    validate => $validate,
  }

}
