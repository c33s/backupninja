# This defined type configures a backupninja backup action.
#
# @param type
#   Defines which type of backup action to install. The value will be used as
#   the configuration file's extension.
#
# @param order
#   Determine the order prefix which will be applied to the configuration
#   filename. Must be a positive integer.
#
# @param ensure
#   Whether the backup action should be present, absent or disabled. When
#   removing or disabling an action, the type and order must match the file to
#   act upon. A disabled action configuration will be kept on the filesystem but
#   will always be ignored by backupninja.
#
# @param content
#   Defines the content of the backup action configuration. Cannot be used in
#   conjuction with $source.
#
# @param source
#   Defines the URL of a file to fetch and install as the backup action.
#
# @param validate
#   Whether to test the backup action before installing it. The test is run
#   with `backupninja --test --now --run`. Be aware that in the case of actions
#   configuration for remote backup, it will attempt to connect to the remote
#   host. If the test fails, the backup action configuration will not be
#   installed/updated.
#
# @example
#   backupninja::action { 'mybackup':
#     type     => 'sh',
#     source   => 'puppet:///modules/profile/backupninja/mybackup.sh',
#     validate => true,
#   }
#
#
define backupninja::action (
  String $type,
  String $filename                              = $name,
  Integer[1] $order                             = 50,
  Enum['present', 'disabled', 'absent'] $ensure = 'present',
  Optional[String] $content                     = undef,
  Optional[String] $source                      = undef,
  Boolean $validate                             = $backupninja::validate_actions,
) {

  include '::backupninja'

  $_type = $ensure ? {
    'disabled' => "${type}.disabled",
    default    => $type
  }

  $_filename ="${order}_${filename}.${_type}"

  if $content and $source {
    fail('backupninja::action cannot have both content and source')
  }

  if !$content and !$source {
    fail('backupninja::action must have either content or source')
  }

  # puppet appends a string to temporary filenames,
  # making backupninja unable to determine the handler
  $validate_cmd = $validate ? {
    true  => @("EOT"/$)
      cp % %.${type}
      FATAL=\$(${backupninja::executable} --test --now --run %.${type} 2>&1 | grep Fatal:)
      rm %.${type}
      if [ -n "\$FATAL" ]; then echo \$FATAL && exit 1; else exit 0; fi
      | EOT
      ,
    false => undef
  }

  file { "${backupninja::configdirectory}/${_filename}":
    ensure       => $ensure,
    owner        => 'root',
    group        => 'root',
    mode         => '0600',
    content      => $content,
    source       => $source,
    validate_cmd => $validate_cmd,
  }

}
