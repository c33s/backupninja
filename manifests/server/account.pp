# This defined type allows nodes to declare a remote account to upload backups,
# leveraging exported ressources (PuppetDB).
#
# By default, a node for which `$backupninja::server` is set to `true` will
# automatically realize all backupninja::server:account resources which are
# tagged with its own FQDN.
#
# @see backupninja::server
#
# @param user
#   Specifies the username of the account.
#
# @param home
#   Specifies the home directory of the account.
#
# @param gid
#   Specifies the gid of the account.
#
# @param manage_homedir
#   Whether to manage the home directory.
#
# @param manage_sshdir
#   Whether to manage the SSH configuration directory.
#
# @param manage_sshkey
#   Whether to manage the public key in ssh_authorized_keys.
#
# @param sshdir
#   Specifies the path to the user account's SSH configuration directory.
#
# @param sshfile
#   Specifies the name of the user account's SSH authorized_key file.
#
# @param sshkey
#   Contains an SSH public key to be deployed in the user account's
#   ssh_authorized_keys file.
#
# @param sshkey_type
#   Specifies the type of the SSH public key defined the the above parameter.
#
# @param sshkey_options
#   Specifies the ssh_authorized_keys options attached to the SSH public key.
#
# @param sshkey_source
#   Specifies a source path for the account's ssh_authorized_keys file. This
#   parameter has no effect if sshkey is defined, and causes sshkey_type and
#   sshkey_options parameters to be ignored.
#
# @param uid
#   Specifies the account's uid.
#
# @example
#   @@backupninja::server::account {
#     "${::fqdn}@backup.example.net":
#       user          => "backup-${::hostname}",
#       home          => "/data/backups/${::fqdn}",
#       sshkey_source => "puppet:///modules/profile/ssh_keys/${::fqdn}.pub",
#       tag           => 'backup.example.net',
#   }
#
define backupninja::server::account (
  String $user,
  String $home,
  String $gid,
  Boolean $manage_homedir                                  = true,
  Boolean $manage_sshdir                                   = true,
  Boolean $manage_sshkey                                   = true,
  Optional[String] $sshdir                                 = "${home}/.ssh",
  Optional[String] $sshfile                                = 'authorized_keys',
  Optional[String] $sshkey                                 = undef,
  Optional[String] $sshkey_type                            = undef,
  Optional[Variant[String, Array[String]]] $sshkey_options = undef,
  Optional[String] $sshkey_source                          = undef,
  Optional[Integer] $uid                                   = undef,
) {

  user { $user:
    ensure     => 'present',
    uid        => $uid,
    gid        => $gid,
    comment    => "${user} backup sandbox",
    home       => $home,
    managehome => true,
    shell      => '/bin/bash',
    password   => '*',
    require    => Group[$gid],
  }

  if $manage_homedir {
    file { $home:
      ensure => directory,
      mode   => '0750',
      owner  => $user,
      group  => 'root',
    }
  }

  if $manage_sshdir {
    file { $sshdir:
      ensure => directory,
      mode   => '0700',
      owner  => $user,
      group  => $gid,
    }
  }

  if $manage_sshkey {

    if empty($sshkey) and empty($sshkey_source) {
      fail('Either sshkey or sshkey_source must be defined.')
    }

    if $sshkey {

      ssh_authorized_key{ $user:
        type    => $sshkey_type,
        options => $sshkey_options,
        key     => $sshkey,
        user    => $user,
        target  => "${sshdir}/${sshfile}",
        require => [ User[$user], File[$sshdir] ],
      }

    }
    else {

      file { "${sshdir}/${sshfile}":
        ensure => present,
        mode   => '0644',
        owner  => 'root',
        group  => 'root',
        source => $sshkey_source,
      }

    }

  }

}
